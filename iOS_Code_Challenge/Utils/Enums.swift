//
//  Enums.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 27/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//


import Foundation

enum ErrorTypes:String
{
    
    case GENERIC_ERROR = "Oops. Service Unavailable, please try again later.." // when api failed due to any network error
    case NONE = "Successfull"
    case PULL_QUERY_ERROR = "There was a problem pulling data from the server."
    case SERVER_ISSUE = "Server was unable to process request, please try again" // when json parsing failed
}
