//
//  MainViewController.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 27/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class MainViewController: UIViewController {
    
    var tableView = UITableView()
    var datalist = [DataDetails]()
    var activityIndicator:NVActivityIndicatorView!
    var textArray = [DataDetails]()
    var imageArray = [DataDetails]()
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.navigationItem.title = "iOS Code Challenge"
        tableView = UITableView(frame: self.view.bounds, style: UITableView.Style.grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.backgroundColor = UIColor.white
        tableView.separatorInset = .zero
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "cell")     // register cell name
        
        let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        tableView.contentInset.top = 20
        tableView.frame = CGRect(x: 0, y: barHeight, width: displayWidth, height: displayHeight - barHeight)
        let contentSize = self.tableView.contentSize
        let footer = UIView(frame: CGRect(x: self.tableView.frame.origin.x,
                                          y: self.tableView.frame.origin.y + contentSize.height,
                                          width: self.tableView.frame.size.width,
                                          height: self.tableView.frame.height - self.tableView.contentSize.height))
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        // add UI elements to view
        view.addSubview(tableView)
        
        // set Activity Indicator
        if #available(iOS 11.0, *) {
            activityIndicator = NVActivityIndicatorView(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 50)/2, y:  (ScreenSize.SCREEN_HEIGHT - 30)/2, width: 50, height: 30), type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.blue, padding: 0)
        } else {
            // Fallback on earlier versions
            activityIndicator = NVActivityIndicatorView(frame: CGRect(x: (ScreenSize.SCREEN_WIDTH - 50)/2, y:  (ScreenSize.SCREEN_HEIGHT - 30)/2, width: 50, height: 30), type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.red, padding: 0)//UIColor(named : "#e8ad00"), padding: 0)
        }
        activityIndicator.center = self.view.center
        activityIndicator.isHidden = true
        // add UI elements to view
        self.view.addSubview(activityIndicator)
        // get data through Api
        self.getData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){                         //Get data via Api
        if ApiManager.checkuser_online() {                  //Check for Network connection
            self.showActivityIndicator()
            //Get data via Api
            ApiManager.getJsonData( completion: { (events, success, error)   in
                
                self.hideActivityIndicator()
                if let list = realm.getObjects(type: DataDetails.self)?.toArray(ofType: DataDetails.self) {
                    
//                    if self.datalist.count == list.count {
//                        return
//                    }
                    self.datalist = list
                    //                    self.eventlist = filteredEvents
                    self.textArray = self.datalist.filter {$0.type == "text"}
                    self.imageArray = self.datalist.filter {$0.type == "image"}
                    self.tableView.reloadData()                      // reload tableview
                    self.viewWillLayoutSubviews()
                }
            })
            
        }else{                      // No Network
            print("No Network")
            // get Data from realm
            if let list = realm.getObjects(type: DataDetails.self)?.toArray(ofType: DataDetails.self) {
                if self.datalist.count == list.count {
                    return
                }
                self.datalist = list
                self.textArray = self.datalist.filter {$0.type == "text"}
                self.imageArray = self.datalist.filter {$0.type == "image"}
                self.tableView.reloadData()
                self.viewWillLayoutSubviews()
            }
        }
    }
    
    func showActivityIndicator()                    // Show activity indicator
    {
        self.view.bringSubview(toFront: activityIndicator)
        activityIndicator.isHidden = false
        UIApplication.shared.beginIgnoringInteractionEvents()
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator()                    // Hide activity indicator
    {
        activityIndicator.isHidden = true
        UIApplication.shared.endIgnoringInteractionEvents()
        activityIndicator.stopAnimating()
    }
  
}
// MARK: - TableView Delegate Datasource Methodes
extension MainViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        
        if indexPath.section == 0 {
            let d = self.textArray[indexPath.row]
            cell.idLable.text = "Id: " + d.id + " Date: " + d.date
            cell.dataLable.text = "Data: " + d.data
            cell.urlImage.isHidden = true
        } else if indexPath.section == 1 {
            let d = self.imageArray[indexPath.row]
            cell.idLable.text = "Id: " + d.id + " Date: " + d.date
            cell.dataLable.text = "Data: " + d.data
            if let url = URL(string: d.data) {
                cell.urlImage.isHidden = false
                cell.urlImage.sd_setImage(with: url, placeholderImage: UIImage(named: "image-placeholder"), options: SDWebImageOptions.refreshCached, completed: nil)
            }else {
                cell.urlImage.isHidden = true
                cell.urlImage.image = nil
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.textArray.count
        }else if section == 1{
            return self.imageArray.count
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return 64
        }else if indexPath.section == 1{
            return 94
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerLabel = UILabel()
        if section == 0 {
            headerLabel.text = "Text Data"
        }else if section == 1{
            headerLabel.text = "Images Data"
        }
        headerLabel.textAlignment = .center
        headerLabel.textColor = .blue
        headerLabel.font = UIFont.boldSystemFont(ofSize: 24)
        return headerLabel
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { // on cell selection show data with details
        tableView.deselectRow(at: indexPath, animated: true)
        let detailsViewController = DetailsViewController()
        if indexPath.section == 0{
            detailsViewController.selectedData = textArray[indexPath.row]
        }else if indexPath.section == 1{
            detailsViewController.selectedData = imageArray[indexPath.row]
        }
        navigationController?.pushViewController(detailsViewController, animated: false)
    }
    
}

// MARK: - TableView cell
class CustomTableViewCell: UITableViewCell {
    
    var urlImage = UIImageView()
    let idLable = UILabel()
    let dataLable = UILabel()
    let timeLable = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //        imgUser.backgroundColor = UIColor.blue
        urlImage = UIImageView(frame: CGRect(x: ScreenSize.SCREEN_WIDTH - 170  , y: 100, width: (ScreenSize.SCREEN_WIDTH - 50), height: 50))
        urlImage.contentMode = .scaleAspectFill // without this your image will shrink and looks ugly
        urlImage.translatesAutoresizingMaskIntoConstraints = false
        urlImage.layer.cornerRadius = 5
        urlImage.clipsToBounds = true
        
        urlImage.translatesAutoresizingMaskIntoConstraints = false
        idLable.translatesAutoresizingMaskIntoConstraints = false
        dataLable.translatesAutoresizingMaskIntoConstraints = false
        timeLable.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(urlImage)
        contentView.addSubview(idLable)
        contentView.addSubview(dataLable)
        contentView.addSubview(timeLable)
        
        let viewsDict = [
            "image" : urlImage,
            "username" : idLable,
            "message" : dataLable,
            "labTime" : timeLable,
            ] as [String : Any]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[image(80)]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[labTime]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[username]-[message]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[username]-[message(280)]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[username]-[image(80)]-|", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[message]-[labTime]-|", options: [], metrics: nil, views: viewsDict))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
