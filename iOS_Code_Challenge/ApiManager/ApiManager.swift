//
//  ApiManager.swift
//  iOS_Code_Challenge
//
//  Created by WSLT82 on 27/10/20.
//  Copyright © 2020 khotbhagya13. All rights reserved.
//


import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import ReachabilitySwift

struct ApiEndPoint
{
    //Get JSON at following API
    static let jsonApi = "https://raw.githubusercontent.com/AxxessTech/Mobile-Projects/master/challenge.json"
}

class ApiManager: NSObject {
    
    class func checkuser_online() -> Bool{          //Check Netwotk Reachability
        
        print("", Reachability()?.currentReachabilityStatus as Any)
        print("",Reachability()!.isReachable)
        return Reachability()!.isReachable
    }
    
    //MARK:- Get all Json data

    class func getJsonData(completion: @escaping (_ data:[JSON]?,_ succeeded:Bool,_ error: ErrorTypes) -> Void)
    {
        let url1 = NSURL(string: ApiEndPoint.jsonApi)
        var mutableR = URLRequest(url: url1! as URL)
        
        mutableR.setValue("application/json", forHTTPHeaderField: "Content-Type")
        mutableR.httpMethod = "GET"
        
        Alamofire.request(mutableR).responseJSON { (responce) in
            
            guard responce.result.error == nil
                else
            {
                DispatchQueue.main.async(execute: {
                    print("----------------------\n")
                    print("---------Error-------------\n", String(data: responce.data!, encoding: .utf8) ?? "not converted")
                    print("----------------------\n")
                    completion(nil,false,ErrorTypes.PULL_QUERY_ERROR)
                })
                return
            }
            if let value: AnyObject = responce.result.value as AnyObject? {
                let json = JSON(value)
                print(json)
                DispatchQueue.main.async(execute: {
                    
                    if let dict = json.array {
                        print(dict)
                        var dataDetails = [DataDetails]()
                        if !dict.isEmpty {
                            if let list = realm.getObjects(type: DataDetails.self)?.toArray(ofType: DataDetails.self) {
                                try! realm.realm.write {
                                    dataDetails = DataDetails.getDataList(dict, isSynced: true)
                                    if list.isEmpty{
                                        realm.saveObjects(objs: dataDetails)
                                    }else{
                                        for d in dataDetails{
                                            
                                            if list.contains(d) {
                                                print("No")
                                                realm.saveSingleObject(obj: d)
                                            }else {
                                                
                                            }
                                            
                                        }
                                    }
                                    //                            realm.saveObjects(objs: dataDetails)
                                    completion(dict, true, ErrorTypes.NONE)
                                }
                            }
                        }else{
                            
                        }
                        
                    }else {
                        completion(nil, false, ErrorTypes.SERVER_ISSUE)
                    }
                })
            }
            else
            {
                DispatchQueue.main.async {
                    completion(nil, false, ErrorTypes.GENERIC_ERROR)
                }
            }
        }
        
    }
}
